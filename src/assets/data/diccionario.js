const config = {
    nameApp: 'Prueba',
    imgPrincipal: '../../../../../assets/dist/img/AdminLTELogo.png'
}

const buttons = {
    editar: 'Editar',
    eliminar: 'Eliminar',
    agregar: 'Agregar elemento',
    agregarRol: 'Agregar Rol',
    agregarEstado: 'Agregar Estado',
    agregarSesion: 'Agregar Sesion',
    agregarUsuario: 'Agregar Usuario',
    agregarMaterial: 'Agg Material',
    agregarPortafolio: 'Agregar Portafolio',
    agregarAutorizacion: 'Agregar Autorizacion',
    agregarCampo: 'Agregar',
    guardar: 'Guardar',
    mostrar: 'Mostrar seleccion',
    regresar: 'Regresar',
    continuar: 'Continuar',
    buscar: 'Buscar',
    procesar: 'Procesar',
}

const estructuraRespuesta = {
    respuesta: 'respuesta'
}

const mensajes = {
    eliminar: '¿Esta seguro que desea eliminar el siguiente elemento?',
    guardar: '¿Esta seguro que desea guardar el siguiente elemento?',
    actualizar: '¿Esta seguro que desea actualizar el siguiente elemento?',
    datoGuardado: 'Se guardo corectamente..!!',
    completeInformacion: 'Complete la informacion requerida',
    mensajeAdvertencia: 'La Órden contiene un solo elemento en el detalle desea eliminar la órden completa?',
    guardarDatorForm: 'Desea guardar los datos del formulario',
    mjsProcesar: 'Desea procesar los datos del formulario',
    mjsCampo: 'Al menos debe contener otro Campo "Articulo Alternativo"',
    mjsCampo1: 'Al menos debe contener otro Campo "CodigoBarra"',
    mjsCampo2: 'Al menos debe contener otro Campo',
    mjsCampoAdv: 'Obligatorio Campo Cantidad',
    mjsFilasCorrectas: 'Filas Correctas',
    mjsFilasIncorrectas: 'Filas Incorrectas',
    procesar: '¿Esta seguro que desea procesar el siguiente formulario?'
}

const buttonsCss = {
    primary: 'btn btn-primary btn-block',
    secondary: 'btn btn-secondary btn-block',
    success: 'btn btn-success btn-block',
    danger: 'btn btn-danger btn-block',
    warning: 'btn btn-warning btn-block',
    info: 'btn btn-info btn-block',
    light: 'btn btn-light btn-block',
    dark: 'btn btn-dark btn-block',

    primaryOutline: 'btn btn-outline-primary btn-block',
    secondaryOutline: 'btn btn-outline-secondary btn-block',
    successOutline: 'btn btn-outline-success btn-block',
    dangerOutline: 'btn btn-outline-danger btn-block',
    warningOutline: 'btn btn-outline-warning btn-block',
    infoOutline: 'btn btn-outline-info btn-block',
    lightOutline: 'btn btn-outline-light btn-block',
    darkOutline: 'btn btn-outline-dark btn-block'
}


export { buttons, mensajes, buttonsCss, estructuraRespuesta, config }