const moduloAmbiente = {
    currentEnvironment: 'DESARROLLO',
    environmentLocal: 'LOCAL',
    environmentDesarrollo: 'DESARROLLO',
    environmentQA: 'QA',
    environmentProduccion: 'PRODUCCION',
    local: {},
    desarrollo: {
        url: 'https://pokemon-pichincha.herokuapp.com/',
    },
    qa: {},
    produccion: {}
}
export { moduloAmbiente }