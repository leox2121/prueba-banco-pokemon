import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/utils/common.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { pokemonModulo } from '../diccinario/pokemonModulo';
import { PokemonModel } from '../model/pokemonModel';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  modulo: any;
  url: string;
  urlPokemon: any;


  constructor(
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.modulo = pokemonModulo;
    this.urlPokemon = this.modulo.url.urlPokemon
    this.url = this._util.getApiPokemon();
  }

  getAllPokemon() {
    return this._commom.peticionGetSinHeader(this.url + this.urlPokemon);
  }

  getPokemonById(idPokemon: string) {
    return this._commom.peticionGetSinHeader(this.url + this.urlPokemon + `${idPokemon}`);
  }

  getPokemonByAuthor(idAuthor: number) {
    return this._commom.peticionGetSinHeader(this.url + this.urlPokemon + `?idAuthor=${idAuthor}`);
  }

  getPokemonNregister(idAuthor: number, count: number) {
    return this._commom.peticionGetSinHeader(this.url + this.urlPokemon + count +`?idAuthor=${idAuthor}`);
  }

  postPokemon(innPokemon: PokemonModel) {
    return this._commom.peticionPostSinHeader(this.url + this.urlPokemon, innPokemon);
  }

  updatePokemon(innPokemon: PokemonModel) {
    return this._commom.peticionPutSinHeader(this.url + this.urlPokemon + `${innPokemon.id}`, innPokemon);
  }

  deletePokemon(innPokemon: PokemonModel) {
    return this._commom.peticionDeleteSinHeader(this.url + this.urlPokemon + `${innPokemon.id}`);
  }
}
