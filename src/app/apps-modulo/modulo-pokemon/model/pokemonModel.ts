export class PokemonModel {

    id?: number;// 7611,
    name: string;// "PatoAmarillo",
    image: string;// 
    type: string;// "fire",
    hp: number;// 100,
    attack: number;// 44,
    defense: number;// 18,
    idAuthor: number;// 1,
    created_at: string;//"2022-06-02T22:26:31.908Z",
    updated_at: string;//"2022-06-07T21:18:42.673Z"

    constructor(){}
    
}