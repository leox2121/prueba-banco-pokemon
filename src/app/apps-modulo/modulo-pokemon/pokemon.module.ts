import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PokemonRoutingModule } from './pokemon-routing.module';
import { FormPokemonComponent } from './views/form-pokemon/form-pokemon.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    FormPokemonComponent
  ],
  exports: [

  ],
  imports: [
    PokemonRoutingModule,
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class PokemonModule { }