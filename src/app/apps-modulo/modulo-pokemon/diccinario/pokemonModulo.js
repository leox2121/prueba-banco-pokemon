const pokemonModulo = {
    url: {
          urlPokemon  : 'pokemons/'
    },
    button: {
        buttonsCss: {
            primary: 'btn btn-primary btn-block',
            secondary: 'btn btn-secondary btn-block',
            success: 'btn btn-success btn-block',
            danger: 'btn btn-danger btn-block',
            warning: 'btn btn-warning btn-block',
            info: 'btn btn-info btn-block',
            light: 'btn btn-light btn-block',
            dark: 'btn btn-dark btn-block',
            primaryOutline: 'btn btn-outline-primary',
            secondaryOutline: 'btn btn-outline-secondary',
            successOutline: 'btn btn-outline-success',
            dangerOutline: 'btn btn-outline-danger',
            warningOutline: 'btn btn-outline-warning',
            infoOutline: 'btn btn-outline-info',
            lightOutline: 'btn btn-outline-light',
            darkOutline: 'btn btn-outline-dark'
        },
        buttonsNombre: {
            editar: 'Editar',
            eliminar: 'Eliminar',
            agregar: 'Agregar ',
            guardar: 'Guardar',
            actualizar: 'Actualizar',
            mostrar: 'Mostrar seleccion',
            regresar: 'Regresar',
            continuar: 'Continuar',
            seleccionar: 'Seleccionar',
            calcular: 'Calcular',
            enviar: 'Enviar',
        },
        buttonsClass: {
            btnNuevoStyle: {
                'float-right': true,
                'mx-3': true
            }
        }
    },
    mensajes: {
        errorServicio: 'error..!!',
        desabilitada: 'Opción desabilitada',
        eliminar: '¿Esta seguro que desea eliminar el siguiente elemento?',
        guardar: '¿Esta seguro que desea guardar el siguiente elemento?',
        actualizar: '¿Esta seguro que desea actualizar el siguiente elemento?',
        calcular: '¿Esta seguro que desea calcular el promedio y la desviación estandar de el siguiente elemento?',
        datoGuardado: 'Se guardo corectamsente..!!',
        guardarParada: 'Esta seguro que desea registrar este evento..??',
        secuencialParada: '¿Esta seguro que desea actualizar los campos de F.Inicio, F.Fin, Contadores y Rechazo de los siguientes elementos?',
        enviar: '¿Esta seguro que desea enviar los siguientes elementos?'
    },
    agGrid: {
        paginacion: 10,
        paginacionPareto: 100,
        id: 'myGrid',
        class: 'ag-theme-material mt-3',
        style: {
            width: '100%',
            height: '600px'
        }
    },
    div: {
        resposiveFull: 'col-12 col-sm-6 col-md-6 col-lg-4 ',//verificar el tipo de pantalla
    },
    dataTable: {
        language_es: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        dtOptions: {
            responsive: true,
            pagingType: 'full_numbers',
            pageLength: 5,
            destroy: true,
            search: true,
            language: {},
            processing: true,
            dom: 'Bfrtip',
            buttons: {
                buttons: [
                    // 'columnsToggle',
                    // 'colvis',
                    // 'copy',
                    // 'print',
                    // 'excel',
                    {
                        text: ' <i class="fa fa-plus" aria-hidden="true"></i> Nuevo',
                        className: 'btn btn-success',
                        titleAttr: null,
                        key: '1',
                        action: null
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Imprimir',
                        className: 'btn btn-primary'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel"></i>',
                        titleAttr: 'Descargar Excel',
                        className: 'btn btn-primary'
                    }
                ],
                dom: {
                    button: {
                        className: 'btn'
                    }
                }
            }
        }
    }
}
export { pokemonModulo }