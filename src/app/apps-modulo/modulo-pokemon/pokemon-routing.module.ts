import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPokemonComponent } from './views/form-pokemon/form-pokemon.component';

const pokemonRoutes: Routes = [
  { path: 'formPokemon', component: FormPokemonComponent }
];

@NgModule({
  imports: [RouterModule.forChild(pokemonRoutes)],
  exports: [RouterModule]
})
export class PokemonRoutingModule { }
