import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PokemonModel } from '../../model/pokemonModel';
import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'app-form-pokemon',
  templateUrl: './form-pokemon.component.html',
  styleUrls: ['./form-pokemon.component.css']
})
export class FormPokemonComponent implements OnInit {

  private ID_AUTHOR: number = 1;

  rowData: PokemonModel[];
  formulario: FormGroup;

  cmbType: any = [
    { clave: 'fire', valor: 'fire' }, { clave: 'water', valor: 'water' }, { clave: 'normal', valor: 'normal' }, { clave: 'bug', valor: 'bug' }, { clave: 'poison', valor: 'poison' },
  ]
  formEditable: boolean;

  textoBuscarEvento: string;

  constructor(
    private fb: FormBuilder,
    private _pokemon: PokemonService
  ) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      id: [''],
      name: ['', [Validators.required]],
      image: ['', [Validators.required]],
      type: ['', [Validators.required]],
      hp: ['', [Validators.required]],
      attack: ['', [Validators.required]],
      defense: ['', [Validators.required]],
      idAuthor: ['', [Validators.required]]
    });
    this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
    this.getPokemon(this.ID_AUTHOR);
  }

  /* OBTIENE TODOS LO REGISTROS DEL SERVICIO*/
  getAllPokemon() {
    this._pokemon.getAllPokemon().subscribe((res: PokemonModel[]) => {
      this.rowData = res;
    });
  }

  /* OBTIENE LOS REGISTROS DEL SERVICIO POR AUTHOR*/
  getPokemon(idAuthor: number) {
    this._pokemon.getPokemonByAuthor(idAuthor).subscribe((res: PokemonModel[]) => {
      console.log(res);
      this.rowData = res;
    }, (err) => {
      console.log(err);
    });
  }

  keyUp() {
    if (this.textoBuscarEvento.length > 0) {
      this.getPokemonById();
    } else {
      this.getPokemon(this.ID_AUTHOR);
    }
  }

  getPokemonById() {
    this._pokemon.getPokemonById(this.textoBuscarEvento).subscribe((res: PokemonModel) => {
      console.log(res);
      this.rowData = [];
      this.rowData.push(res);
    }, (err) => {
      console.log(err);
      this.rowData = [];
    });
  }

  nuevo() {
    this.formulario.reset();
    this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
  }

  editar(item: PokemonModel) {
    this.formEditable = true;
    const pokemonEdit = {
      id: item.id,
      name: item.name,
      image: item.image,
      type: item.type,
      hp: item.hp,
      attack: item.attack,
      defense: item.defense,
      idAuthor: item.idAuthor,
    }
    this.formulario.setValue(pokemonEdit)
  }

  accion() {
    if (this.formEditable) {
      this.update();
    } else {
      this.guardar();
    }
  }

  guardar() {
    this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
    const pokemon: PokemonModel = this.formulario.value;
    this._pokemon.postPokemon(pokemon).subscribe((res: any) => {
      console.log(res);
      this.getPokemon(this.ID_AUTHOR);
      this.formulario.reset();
      this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
    }, (err) => {
      console.log(err);
    });
  }

  update() {
    this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
    const pokemon: PokemonModel = this.formulario.value;
    this._pokemon.updatePokemon(pokemon).subscribe((res: any) => {
      this.getPokemon(this.ID_AUTHOR);
      this.formEditable = false;
      this.formulario.reset();
      this.formulario.get('idAuthor').setValue(this.ID_AUTHOR);
    }, (err) => {
      console.log(err);
    });
  }

  eliminar(item: PokemonModel) {
    console.log(item);
    this._pokemon.deletePokemon(item).subscribe((res: any) => {
      console.log(res);
      this.rowData = this.rowData.filter(e => e.id !== item.id);
    }, (err) => {
      console.log(err);
    });
  }
}
