import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// componentes
import { CommonModule } from '@angular/common';
import { ComboBoxComponent } from './components/combo-box/combo-box.component';
import { ErrorMsgComponent } from './components/error-msg/error-msg.component';
import { InputComponent } from './components/input/input.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ModalComponent } from './components/modal/modal.component';
import { CardComponent } from './components/card/card.component';
import { BotonEditarGridComponent } from './components/boton-editar-grid/boton-editar-grid.component';
import { ContentComponent } from './components/content/content.component';


@NgModule({
  declarations: [
    ContentComponent,
    ComboBoxComponent,
    ErrorMsgComponent,
    InputComponent,
    LoadingComponent,
    ModalComponent,
    CardComponent,
    BotonEditarGridComponent
  ],
  exports: [
    ContentComponent,
    ComboBoxComponent,
    ErrorMsgComponent,
    InputComponent,
    LoadingComponent,
    ModalComponent,
    CardComponent,
    BotonEditarGridComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})

export class SharedModule {}