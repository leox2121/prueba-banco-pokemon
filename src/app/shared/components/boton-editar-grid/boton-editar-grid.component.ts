import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-boton-editar-grid',
  templateUrl: './boton-editar-grid.component.html',
  styleUrls: ['./boton-editar-grid.component.css']
})
export class BotonEditarGridComponent  {

  params;
  label: string;
  colorBoton: string; 
  iconEdit: string = 'fas fa-edit';
  iconDelete: string = 'fa fa-trash';
  iconVer: string = 'fa fa-eye';
  iconSelect: string = 'fas fa-clipboard-check';
  iconGraphi: string = 'fas fa-chart-pie';
  iconProcess: string = "fas fa-calculator";
  iconAdd: string = "fa fa-plus";

  agInit(params): void {
    this.params = params;
    this.label = this.params.label || null;
    this.colorBoton = this.params.colorBoton || null;
  }

  refresh(params?: any): boolean {
    return true;
  }

  onClick($event) {
    if (this.params.onClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      };
      this.params.onClick(params);

    }
  }

}