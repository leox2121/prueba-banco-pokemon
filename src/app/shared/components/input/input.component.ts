import { Component, Input, forwardRef } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const INPUT_FIELD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputComponent),
  multi: true
};

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [INPUT_FIELD_VALUE_ACCESSOR]
})

export class InputComponent implements ControlValueAccessor  {

  @Input() placeholder?: string;
  @Input() label: string;
  @Input() control: FormControl;
  @Input() isReadOnly = false;
  @Input() type = 'text';
  @Input() id: string;
  @Input() claseLabel = '';

  @Input() mostrarImg = false;
  @Input() rutaImagen: string = '';
  //claseDanger

  constructor() {
    if (!this.placeholder) {
      this.placeholder = '';
    }
  }

  private innerValue: any;

  get value() {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCb(v);
    }
  }

  onChangeCb: (_: any) => void = () => {};
  onTouchedCb: (_: any) => void = () => {};

  writeValue(v: any): void {
    this.value = v;
  }

  registerOnChange(fn: any): void {
    this.onChangeCb = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCb = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isReadOnly = isDisabled;
  }

}
