import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  @Input() innTemplate: TemplateRef<any>;
  @Input() tituloCard: string = 'Sin titulo';
  @Input() titulo: string = 'Sin titulo';
  @Input() modulo: string = 'Sin titulo';

  classefecto:string = "main-container animated fadeInRight";
  
  constructor() { }

  ngOnInit(): void {
  }

}
