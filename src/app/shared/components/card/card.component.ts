import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() innTemplate: TemplateRef<any>;
  @Input() titulo: string = 'Sin titulo';
  
  constructor() { }

  ngOnInit(): void {
  }

}
