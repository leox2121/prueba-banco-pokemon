import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() innTemplate: TemplateRef<any>;
  @Input() titulo = 'Sin titulo';
  @Input() idButton: string;
  @Input() idOpen: string;
  @Input() idClose: string;
  @Input() classHeader: string = '';
  constructor() { }

  ngOnInit() {
  }

}
