import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, timeout } from 'rxjs/operators';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private http: HttpClient
  ) { }

  // TODO: PETICIONES SIN HEADER
  // *METODO PARA REALIZAR PETICIONES GET SIN NECESIDAD DE UN TOKEN 
  peticionGetSinHeader(innUrl: any) {
    return this.http.get(innUrl);
  }

  // *METODO PARA REALIZAR PETICIONES POST SIN NECESIDAD DE UN TOKEN 
  peticionPostSinHeader(innUrl: string, innObjeto) {
    return this.http.post<any>(innUrl, innObjeto);
  }

  // *METODO PARA REALIZAR PETICIONES PUT SIN NECESIDAD DE UN TOKEN 
  peticionPutSinHeader(innUrl: string, innObjeto) {
    return this.http.put<any>(innUrl, innObjeto);
  }

  // *METODO PARA REALIZAR PETICIONES DELETE SIN NECESIDAD DE UN TOKEN 
  peticionDeleteSinHeader(innUrl: string, innObjeto?) {
    return this.http.delete<any>(innUrl, innObjeto);
  }
}


