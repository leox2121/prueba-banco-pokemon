import { Injectable } from '@angular/core';
import { estructuraRespuesta } from '../../../assets/data/diccionario';
import { moduloAmbiente } from '../../../assets/ambiente/ambienteModulo';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  mdAmbiente: any = {};
  ambiente: string;

  innEstructuraRespuesta: any;

  urlPokemon: any;
  
  constructor() {
    this.innEstructuraRespuesta = estructuraRespuesta.respuesta;
    this.mdAmbiente = moduloAmbiente;
    this.ambiente = this.mdAmbiente.currentEnvironment;
    switch (this.ambiente) {
      case this.mdAmbiente.environmentLocal:
        this.urlPokemon = this.mdAmbiente.local.url;
        break;
      case this.mdAmbiente.environmentDesarrollo:
        this.urlPokemon = this.mdAmbiente.desarrollo.url;
        break;
      case this.mdAmbiente.environmentQA:
        this.urlPokemon = this.mdAmbiente.qa.url;
        break;
      case this.mdAmbiente.environmentProduccion:
        this.urlPokemon = this.mdAmbiente.produccion.url;
        break;
      default:
        break;
    }
  }

  getApiPokemon() {
    const url = `${this.urlPokemon}`;
    return url;
  }

}
